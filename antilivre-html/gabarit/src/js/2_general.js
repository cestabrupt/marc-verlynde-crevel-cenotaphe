// Smooth Scroll
//
var scroll = new SmoothScroll('a[href*="#"]', {
  speed: 3000,
  speedAsDuration: true,
  easing: 'easeInOutQuint'
});
