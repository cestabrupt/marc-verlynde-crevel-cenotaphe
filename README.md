# ~/ABRÜPT/MARC VERLYNDE/CREVEL, CÉNOTAPHE/*

La [page de ce livre](https://abrupt.ch/marc-verlynde/crevel-cenotaphe/) sur le réseau.

## Sur le livre

La tradition littéraire du tombeau paraît mal enterrée. Peut-être. *Crevel, Cénotaphe* en pastiche non tant la dévotion que le désir d’un portrait posthume, vivant. D’où l’idée de dresser un cénotaphe pour Crevel, cette présence indissociable du surréalisme, ce visage de sa vague de rêve mais aussi ce dandy suicidé, écrivain révolté et engagé. Dans cette brève évocation à la dérive, dans un désordre voulu pour laisser apparaître les images et les strates d’une personnalité contradictoire, Marc Verlynde fait de ce cénotaphe la représentation d’une <a href="https://viduite.wordpress.com/" target="_blank">viduité</a>, d’un portrait du poète partout où le corps échappe. Loin de la biographie, de l’essai universitaire, *Crevel, Cénotaphe* est un portrait-puzzle, collage de citations ou d’emprunts, pour laisser perdurer le fantôme sensible de l’auteur de *Mon corps et moi*, du *Clavecin de Diderot* ou encore de si décisifs <a href="https://abrupt.ch/rene-crevel/dali-anti-obscurantiste/" class="shake">articles sur Dalí</a>. 

## Sur l'auteur

Marc Verlynde est né en 1981, il vit actuellement à Nantes où il tient un <a href="https://viduite.wordpress.com/" target="_blank">blog de critique littéraire</a>.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
